<?php

declare(strict_types = 1);

namespace Drupal\group_comment\Plugin\Group\RelationHandler;

use Drupal\group\Plugin\Group\RelationHandler\AccessControlInterface;
use Drupal\group\Plugin\Group\RelationHandler\AccessControlTrait;


/**
 * Provides group access control for group_comment relation plugin.
 */
class GroupCommentAccessControl implements AccessControlInterface  {

  use AccessControlTrait;

  /**
   * Constructs a new GroupCommentAccessControl.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\AccessControlInterface $parent
   *   The parent access control handler.
   */
  public function __construct(AccessControlInterface $parent) {
    $this->parent = $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsOperation($operation, $target): bool {
    // Close access to directly create comments in groups using add form. This
    // is because comments are being attached to groups automatically.
    // @See group_comment_entity_insert.
    if ($operation === 'create' && $target === 'entity') {
      return FALSE;
    }
    return $this->parent->supportsOperation($operation, $target);
  }

}
