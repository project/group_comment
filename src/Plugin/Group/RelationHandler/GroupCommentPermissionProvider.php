<?php

declare(strict_types = 1);

namespace Drupal\group_comment\Plugin\Group\RelationHandler;

use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface;
use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderTrait;


/**
 * Provides group permissions for group_comment relation plugin.
 */
class GroupCommentPermissionProvider implements PermissionProviderInterface  {

  use PermissionProviderTrait;

  /**
   * Constructs a new GroupCommentPermissionProvider.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface $parent
   *   The default permission provider.
   */
  public function __construct(PermissionProviderInterface $parent) {
    $this->parent = $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermission($operation, $target, $scope = 'any'): string|false {
    if ($target === 'relationship') {
      // Cannot add an existing comment entity to the group through the UI.
      if ($operation === 'create') {
        return FALSE;
      }
      // Cannot update comment relation through the UI.
      elseif ($operation === 'update') {
        return FALSE;
      }
      // Cannot delete comment relation through the UI.
      elseif ($operation === 'delete') {
        return FALSE;
      }
    }
    // The "view $scope unpublished $this->pluginId entity" won't work for
    // comment entity. This is because of the implementation of drupal core.
    // @see \Drupal\comment\CommentStorage::loadThread
    // @todo remove this method when https://www.drupal.org/project/drupal/issues/2980951
    //   is in.
    if ($operation === 'view unpublished' && $target === 'entity' && $scope === 'any') {
      return FALSE;
    }

    return $this->parent->getPermission($operation, $target, $scope);
  }

  /**
   * Gets the name of the skip comment approval for the entity.
   *
   * @return string|false
   *   The permission name or FALSE if it does not apply.
   */
  public function getEntitySkipCommentApprovalPermission(): string|false {
    if ($this->definesEntityPermissions) {
      return "skip comment approval $this->pluginId entity";
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPermissions(): array {
    $permissions = $this->parent->buildPermissions();

    // Provide permission for skipping comment approval.
    $prefix = 'Entity:';
    if ($name = $this->getEntitySkipCommentApprovalPermission()) {
      $permissions[$name] = [
        'title' => "$prefix Skip comment approval",
      ];
    }

    return $permissions;
  }

}
