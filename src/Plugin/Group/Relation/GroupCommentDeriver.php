<?php

declare(strict_types = 1);

namespace Drupal\group_comment\Plugin\Group\Relation;

use Drupal\comment\Entity\CommentType;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;

/**
 * Driver for comment group relation.
 */
class GroupCommentDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (CommentType::loadMultiple() as $name => $comment_type) {
      $label = $comment_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', $this->t('Group comment (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', $this->t('Adds %type comments to groups.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
